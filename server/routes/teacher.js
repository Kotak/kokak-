const express = require('express'),
	router = express.Router(),
	jwt = require('jsonwebtoken'),
	uniqid = require('uniqid'),
	User = require('./../models/User');
	Student = require('./../models/Student'),	
	Class = require('./../models/Class'),
	School = require('./../models/School'),
	Student = require('./../models/Student'),
	Attendence = require('./../models/Attendence'),
	Calendar = require('./../models/Calendar'),	

module.exports = function(passport) {

	router.get('/profile', passport.authenticate('jwt', {session:false}), function(req,res) {
		let findUser = User.findById(req.user._id).populate('school').populate('class').exec();
		findUser.then(function(user) {
			res.status(200).json({
				id: user._id,
				firstName : user.firstName,
				lastName : user.lastName,
	  			gender: user.gender,
	  			schoolid: user.school.schoolId,
	  			nationalid: user.nationalId,
	  			telephone: user.telephoneNumber,
	  			classname: user.class.name
			})
		}).catch(function(error) {
			return res.status(400).json({message: "Error in fetching from database"});			
		})
	})

	router.put('/profile', passport.authenticate('jwt', {session:false}), function(req,res) {
		let findUser = User.findOneAndUpdate({_id: req.user._id}, {$set: {firstName: req.body.firstname, lastName: req.body.lastname, gender: req.body.gender, nationalId: req.body.nationalid, telephoneNumber: req.body.contact}}, {new: true}).populate('school').populate('class').exec();
		findUser.then(function(user) {
			res.status(200).json({
				id: user._id,
				firstName : user.firstName,
				lastName : user.lastName,
	  			gender: user.gender,
	  			schoolid: user.school.schoolId,
	  			nationalid: user.nationalId,
	  			telephone: user.telephoneNumber,
	  			classname: user.class.name 
			})
		}).catch(function(error) {
			return res.status(400).json({message: "Error in fetching from database"});			
		})
	})

	router.get('/students/:id', passport.authenticate('jwt', {session: false}), function(req, res) {
		let findStudent = Student.findById(req.params.id).populate('class').exec();
		findStudent.then(function(student) {
			res.status(200).json({
				firstname: student.firstName,
				lastname: student.lastName,
				gender: student.gender,
				dateofbirth: "" + student.birthMonth + "/" + student.birthDay + "/" + student.birthYear,
				guardian: student.parentName,
				telephone: student.telephoneNumber,
				studentid: student.studentId,
				nationalid: student.nationalId,
				avegrade: student.averageGrade,
				shoesize: student.shoeSize 
			})
		})
	})

	router.put('/students/:id', passport.authenticate('jwt', {session:false}), function(req,res) {
        let birthArr = req.body.dob.split("/");
        let birthMonth = Number((birthArr[0][0] == "0") ? birthArr[0][1] : birthArr[0]);
        let birthDay = Number(birthArr[1]);
        let birthYear = Number(birthArr[2]);
        const updateStudent = Student.findOneAndUpdate({_id: req.params.id}, {$set: {firstName: req.body.firstname, lastName: req.body.lastname, birthMonth: birthMonth, birthDay: birthDay, birthYear: birthYear, gender: req.body.gender, parentName: req.body.guardian, telephoneNumber: req.body.contact, nationalId: req.body.nationalid, averageGrade: req.body.grade, shoeSize: req.body.shoesize}}, {new: true}).exec();
		updateStudent.then(function(student) {
			return res.status(200).json({
				firstname: student.firstName,
				lastname: student.lastName,
				gender: student.gender,
				dateofbirth: "" + student.birthMonth + "/" + student.birthDay + "/" + student.birthYear,
				guardian: student.parentName,
				telephone: student.telephoneNumber,
				studentid: student.studentId,
				nationalid: student.nationalId,
				avegrade: student.averageGrade,
				shoesize: student.shoeSize   			
			})
		}).catch(function(error) {
			return res.status(400).json({success: false, message: "Error in updating database"});
		})
	})	

	router.delete('/students/:id', passport.authenticate('jwt', {session: false}), function(req, res) {
		const currDate = new Date();
		const midNight = new Date(Math.floor(currDate.getTime() / (1000*60*60*24))*1000*60*60*24);			
		let updateStudent = Student.findOneAndUpdate({_id: req.params.id}, {$set: {archived: true}}, {new: true}).exec();
		let updateClass = Class.findOneAndUpdate({_id: req.user.class}, {$pull: {students: req.params.id}}, {new: true}).exec();
		let updateSchool = School.findOneAndUpdate({_id: req.user.school}, {$pull: {students: req.params.id}}, {new: true}).exec();
		let updateAttendence = Attendence.findOneAndUpdate({class: req.user.class, date: midNight}, {$pull: {studentsPresent: req.params.id, studentsNotPresent: req.params.id}}, {new: true}).exec();
		let updateCalendar = updateAttendence.then(async function(updatedAttendence) {
			let attendences = await Attendence.find({school: req.user.school, date: midNight}).exec();
			let totalAttendence = 0;
			for (let i=0; i<attendences.length; i++) {
				totalAttendence += attendences[i].studentsPresent.length;
			}
			let updatedCalendar = await Calendar.findOneAndUpdate({school: req.user.school, date: midNight}, {$set: {attendence: totalAttendence}}, {new: true});
			return updatedCalendar;
		}).catch(function(error) {
			return error;
		})
		Promise.all([updateStudent,updateClass,updateSchool, updateAttendence, updateCalendar]).then(function(results) {
			if (results[0] instanceof Error || results[1] instanceof Error || results[2] instanceof Error || results[3] instanceof Error || results[4] instanceof Error) {
				throw Error;
			} else {
				return res.status(200).json({success: true})
			}
		}).catch(function(error) {
			return res.status(400).json({success: false, message: "Error in updating database"});
		})
	})


	router.get('/students', passport.authenticate('jwt', {session:false}), function(req,res) {
		const currDate = new Date();
		const midNight = new Date(Math.floor(currDate.getTime() / (1000*60*60*24))*1000*60*60*24);	
		let getAttendence = Attendence.findOne({class: req.user.class, date: midNight}).populate('class').populate('studentsPresent').populate('studentsNotPresent').select('studentsPresent studentsNotPresent').exec();
		getAttendence.then(function(attendence) {
			if (!attendence) {
				return res.status(200).json({class: "", students: []});
			}
			let i = 0, 
				j = 0,
				attendenceList = [];
			const plength = attendence.studentsPresent.length,
				  nplength = attendence.studentsNotPresent.length;
			let studentsPresent = attendence.studentsPresent;
			let studentsNotPresent = attendence.studentsNotPresent;
			while (i != plength && j != nplength) {
				if (studentsPresent[i].firstName+studentsPresent[i].lastName < studentsNotPresent[j].firstName+studentsNotPresent[j].lastName) {
					attendenceList.push({id: studentsPresent[i]._id, firstName: studentsPresent[i].firstName, lastName: studentsPresent[i].lastName, attending: true});
					i++;
				} else {
					attendenceList.push({id: studentsNotPresent[j]._id, firstName: studentsNotPresent[j].firstName, lastName: studentsNotPresent[j].lastName, attending: false});
					j++;
				}
			}
			if (i == studentsPresent.length) {
				studentsNotPresent.slice(j).forEach(function(student) {
					attendenceList.push({id: student._id, firstName: student.firstName, lastName: student.lastName, attending: false});
				})
			} else {
				studentsPresent.slice(i).forEach(function(student) {
					attendenceList.push({id: student._id, firstName: student.firstName, lastName: student.lastName, attending: true});
				})
			}
			return res.status(200).json({class: attendence.class.name, students: attendenceList});
		}).catch(function(error) {
			return res.status(400).json({success: false, message: "Error in fetching from database"});
		})
	})

	router.post('/students', passport.authenticate('jwt', {session: false}), function(req, res) {
        let birthArr = req.body.dob.split("/");
        let birthMonth = Number((birthArr[0][0] == "0") ? birthArr[0][1] : birthArr[0]);
        let birthDay = Number(birthArr[1]);
        let birthYear = Number(birthArr[2]);
    	let newStudent = new Student({
    		firstName: req.body.firstname,
    		lastName: req.body.lastname,
    		teacher: req.user._id,
    		class: req.user.class,
    		birthMonth: birthMonth,
    		birthDay: birthDay,
    		birthYear: birthYear,
    		gender: req.body.gender,
    		parentName: req.body.guardian,
    		telephoneNumber: req.body.contact,
    		nationalId: req.body.nationalid,
    		studentId: uniqid(),
    		averageGrade: req.body.avegrade,
    		shoeSize: req.body.shoesize    		
    	})
        let updateClass = newStudent.save().then(async function(student) {
        	if (student instanceof Error) {
        		throw student;
        	} else {
				const currDate = new Date();
				const midNight = new Date(Math.floor(currDate.getTime() / (1000*60*60*24))*1000*60*60*24);        		
	        	await Class.findOneAndUpdate({_id: student.class}, {$push: {students: student._id}},{new: true}).exec();
	        	await Attendence.findOneAndUpdate({class: req.user.class, date: midNight}, {$push: {studentsNotPresent: student._id}}).exec();
	        	return student     
        	}
        }).catch(function(error) {
        	return error;
        })
        let updateSchool = updateClass.then(async function(student) {
        	if (student instanceof Error) {
        		throw student;
        	} else {
	        	await School.findOneAndUpdate({name: req.body.schoolname}, {$push: {students: student._id}}, {new: true}).exec();
	        	return student;
        	}
        }).catch(function(error) {
        	throw error;
        })
        updateSchool.then(function(student) {
        	res.status(200).json({
				firstname: student.firstName,
				lastname: student.lastName,
				gender: student.gender,
				dateofbirth: "" + student.birthMonth + "/" + student.birthDay + "/" + student.birthYear,
				guardian: student.parentName,
				telephone: student.telephoneNumber,
				studentid: student.studentId,
				nationalid: student.nationalId,
				avegrade: student.averageGrade,
				shoesize: student.shoeSize        		
        	})
        }).catch(function(error) {
        	res.status(400).json({success: false, message: "Error in updating database"});
        })
	})

	router.put('/students', passport.authenticate('jwt', {session:false}), function(req,res) {		
		const currDate = new Date();
		const midNight = new Date(Math.floor(currDate.getTime() / (1000*60*60*24))*1000*60*60*24);
		let updateAttendence = Attendence.findOneAndUpdate({class: req.user.class, date: midNight}, {$set: {studentsPresent: req.body.attending, studentsNotPresent: req.body.notAttending}}, {new: true}).exec();	
		let getAttendences = updateAttendence.then(async function(updatedAttendence) {
			let totalAttendence = 0;
			let attendences = await Attendence.find({school: req.user.school, date: midNight});
			attendences.forEach(function(attendence) {
				totalAttendence += attendence.studentsPresent.length;
			})
			return totalAttendence;
		})
		getAttendences.then(async function(totalAttendence) {
			let updatedCalendar = await Calendar.findOneAndUpdate({school: req.user.school, date: midNight}, {attendence: totalAttendence} , {new: true}).exec();
			return res.status(200).json({success: true});
		}).catch(function(error) {
			return res.status(400).json({success: false, message: "Error in fetching from database"});
		})
	})	


	router.delete('/students/:id', passport.authenticate('jwt', {session:false}), async function(req,res) {
		let updateStudent = await Student.findOneAndUpdate({_id: req.params.id}, {$set: {archived: true}}).exec();
		let updateClass = await Class.findOneAndUpdate({_id: req.user.class}, {$pull: {students: req.params.id}}).exec();
		let updateSchool = await School.findOneAndUpdate({_id: req.user.school}, {$pull: {students: req.params.id}}).exec();
		Promise.all([updateStudent, updateClass, updateSchool]).then(function(results) {
			res.status(200).json({success: true});
		}).catch(function(error) {
			res.status(401).json({succss: false, message: "Error in updating database."});
		})
	})	

	return router;
}