package com.example.app.models;

import java.util.HashMap;

//@org.parceler.Parcel
public class Food {

     String mFoodName;
     HashMap<String, String> mIngredientRatios;

    public Food() {

    }

    public Food(String name, HashMap<String, String> ingredientRations) {
        mFoodName = name;
        mIngredientRatios = ingredientRations;
    }


    @Override
    public String toString() {
        return "Name: " + mFoodName + " " + "Ratios: " + mIngredientRatios.toString();
    }

    public String getFoodName() {
        return mFoodName;
    }

    public HashMap getRatios() {
        return mIngredientRatios;
    }
}
