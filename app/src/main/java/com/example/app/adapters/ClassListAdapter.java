package com.example.app.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.app.R;
import com.example.app.models.Class;

import java.util.ArrayList;

public class ClassListAdapter extends RecyclerView.Adapter<ClassListAdapter.ClassListViewHolder> {

    private ArrayList<Class> mClassList;
    private Context mContext;

    public ClassListAdapter(Context context, ArrayList<Class> classes) {
        mClassList = classes;
        mContext = context;
    }

    @Override
    public void onBindViewHolder(@NonNull ClassListViewHolder viewHolder,
                                 int position) {
        Class thisclass = mClassList.get(position);
        Log.v("classname", thisclass.getName());
        Log.v("classsize", String.valueOf(thisclass.getClassSize()));
        viewHolder.mClassName.setText(thisclass.getName());
        viewHolder.mSize.setText(String.valueOf(thisclass.getClassSize()));
    }

    @NonNull
    @Override
    public ClassListViewHolder onCreateViewHolder(@NonNull ViewGroup group, int i) {
        View classview = LayoutInflater.from(group.getContext()).inflate(R.layout.double_item_view, group, false);
        return new ClassListViewHolder(classview);
    }

    @Override
    public int getItemCount() {
        return mClassList.size();
    }

    public Class getSelectedClass(int position) {
        return mClassList.get(position);
    }


    protected class ClassListViewHolder extends RecyclerView.ViewHolder {

        private TextView mClassName;
        private TextView mSize;

        public  ClassListViewHolder (View itemview) {
            super(itemview);
            mClassName = (TextView) itemview.findViewById(R.id.item_name);
            mSize = (TextView) itemview.findViewById(R.id.secitem_name);
        }
    }


}
