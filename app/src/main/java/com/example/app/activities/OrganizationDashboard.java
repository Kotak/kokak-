package com.example.app.activities;

import android.content.Intent;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.app.R;
import com.example.app.adapters.SchoolListAdapter;
import com.example.app.asynctasks.HttpGetRequests;
import com.example.app.fragments.InformationType;
import com.example.app.fragments.WarningDialog;
import com.example.app.interfaces.CallbackListener;
import com.example.app.models.Calendar;
import com.example.app.models.Class;
import com.example.app.models.Food;
import com.example.app.models.School;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

//import org.parceler.Parcels;

import java.lang.reflect.Type;
import java.util.ArrayList;

import static com.example.app.util.Constants.GET_CLASSES;
import static com.example.app.util.Constants.GET_DAILY_ATTENDANCE;
import static com.example.app.util.Constants.GET_DAILY_FOOD;
import static com.example.app.util.Constants.GET_MONTHLY_ATTENDANCE;
import static com.example.app.util.Constants.GET_MONTHLY_FOOD;
import static com.example.app.util.Constants.GET_SCHOOLS;
import static com.example.app.util.Constants.REQUEST_CLASS_LIST;
import static com.example.app.util.Constants.REQUEST_FOOD_INFO;
import static com.example.app.util.Constants.REQUEST_ORG_DASHBOARD;

public class OrganizationDashboard extends AppCompatActivity implements AdapterView.OnItemClickListener,
        CallbackListener, WarningDialog.OnFragmentInteractionListener {

    private School mSelectedSchool;
    private int mRequestCode;
    private ArrayList<School> mSchoolList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_organization_dashboard);
        Gson gson = new Gson();
        String schoolString = getIntent().getStringExtra("SchoolList");
        if (schoolString != null) {
            Type type = new TypeToken<ArrayList<School>>() {}.getType();
            mSchoolList = gson.fromJson(schoolString, type);
        }
        //ArrayList<School> mSchoolList = Parcels.unwrap(getIntent().getParcelableExtra("SchoolList"));
        ListView mSchools = (ListView) findViewById(R.id.school_listview);
        SchoolListAdapter mAdapter = new SchoolListAdapter(this, mSchoolList);
        mSchools.setAdapter(mAdapter);
        mSchools.setOnItemClickListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        mSelectedSchool = (School) parent.getItemAtPosition(position);
        InformationType fragment = new InformationType();
        FragmentManager manager = getSupportFragmentManager();
        fragment.show(manager, "infotype");
    }

    @Override
    public void onCompletionHandler(boolean success, int requestcode, Object obj) {
        if (success) {
            switch (requestcode) {
                case GET_CLASSES:
                    ArrayList<Class> mClassList = (ArrayList<Class>) obj;
                    mSelectedSchool.addClasses(mClassList);
                    break;

                case GET_DAILY_FOOD:
                    ArrayList<Calendar> mDailyFood = (ArrayList<Calendar>) obj;
                    mSelectedSchool.addAllCalendars(mDailyFood);
                    Log.v("thisschool", mSelectedSchool.getSchoolName());
                    Log.v("thisclasses", mSelectedSchool.getClasses().toString());
                    Log.v("thiscalendars", mSelectedSchool.getCalendars().toString());
                    Gson gson = new Gson();
                    Type type = new TypeToken<School>() {}.getType();
                    String schoolString = gson.toJson(mSelectedSchool, type);
                    mRequestCode = GET_DAILY_ATTENDANCE;
                    Intent intent = new Intent(this, SchoolInfoActivity.class);
                    intent.putExtra("SelectedSchoolInfo", schoolString);
                    intent.putExtra("RequestCode", mRequestCode);
                    intent.putExtra("TypeOfInfo", "Daily");
                    startActivity(intent);
                    break;

                case GET_MONTHLY_FOOD:
                    ArrayList<Calendar> mMonthlyFood = (ArrayList<Calendar>) obj;
                    mSelectedSchool.addAllCalendars(mMonthlyFood);
                    Intent monthlyintent = new Intent(this, SchoolInfoActivity.class);
                    Gson monthlygson = new Gson();
                    Type monthlytype = new TypeToken<School>() {}.getType();
                    String monthlyschoolString = monthlygson.toJson(mSelectedSchool, monthlytype);
                    mRequestCode = GET_MONTHLY_ATTENDANCE;
                    monthlyintent.putExtra("SelectedSchoolInfo", monthlyschoolString);
                    monthlyintent.putExtra("RequestCode", mRequestCode);
                    monthlyintent.putExtra("TypeOfInfo", "Monthly");
                    startActivity(monthlyintent);
                    break;

            }
        }
    }

    @Override
    public void onIteraction(int code) {
        HttpGetRequests task = new HttpGetRequests(GET_CLASSES, this, this);
        task.execute(REQUEST_CLASS_LIST + "/" + mSelectedSchool.getSchoolID() + "/" + "classes");
        switch (code) {
            case 0:
                HttpGetRequests feedingtask = new HttpGetRequests(GET_DAILY_FOOD, this, this);
                feedingtask.execute(REQUEST_FOOD_INFO + "/" + mSelectedSchool.getSchoolID() + "/" + "food");
                break;
            case 1:
                HttpGetRequests monthlytask = new HttpGetRequests(GET_MONTHLY_FOOD, this, this);
                monthlytask.execute(REQUEST_FOOD_INFO + "/" + mSelectedSchool.getSchoolID() + "/" + "food");
                break;
        }
    }
}

