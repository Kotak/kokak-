package com.example.app.activities;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.app.R;
import com.example.app.asynctasks.HttpPostRequests;
import com.example.app.fragments.SuccessDialog;
import com.example.app.interfaces.CallbackListener;

import java.util.HashMap;

import static com.example.app.util.Constants.POST_NEW_CONTACT;
import static com.example.app.util.Constants.RESET_CONTACT;

public class UpdateContactActivity extends AppCompatActivity implements View.OnClickListener, CallbackListener {

    private EditText mNewNumber;

    private EditText mOldnumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_contact);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) actionBar.hide();

        Button mSubmitButton = (Button) findViewById(R.id.updatecontact_submit_button);
        mNewNumber = (EditText) findViewById(R.id.newnumber_edittext);
        mOldnumber = (EditText) findViewById(R.id.old_phone_number_edittext);
        mSubmitButton.setOnClickListener(this);
    }

    //TODO: Set url for updating contact
    @Override
    public void onClick(View view) {
        HashMap<String, String> postdata = new HashMap<>();
        postdata.put("newNumber", mNewNumber.getText().toString());
        postdata.put("oldNumber", mOldnumber.getText().toString());
        HttpPostRequests task = new HttpPostRequests(postdata, POST_NEW_CONTACT, this, this);
        task.execute(RESET_CONTACT);

    }

    @Override
    public void onCompletionHandler(boolean success, int requestcode, Object obj) {
        if (success) {
            Fragment successIndicator = SuccessDialog.newInstance("Number Successfully Updated");
            getSupportFragmentManager().beginTransaction()
                    .add(successIndicator, "SuccessFragment")
                    .commit();
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
        }
    }
}
